// variable getCube
let getCube = Math.pow(2,3);
console.log(`The cube of 2 is ${getCube}`);

// =============================================
// variable address
const address = ["258 Washington Ave NW","California 90011"];
const [firstAddress, lastAddress] = address;
console.log(`I live at ${firstAddress}, ${lastAddress}`);

// =============================================
// variable animal
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	kilo: 1075,
	feet: 20,
	inches: 3
}
const {name, kilo, feet, inches, species} = animal;
console.log(`${name} was a ${species}. He weighted at ${kilo} kgs with a measurement of ${feet} ft ${inches} in.`);

// =============================================
// array of numbers
const arrayNumbers = [1,2,3,4,5];
arrayNumbers.forEach((number) => console.log(number));

// =============================================
// variable reduceNumber
let reduceNumber = arrayNumbers.reduce((total,num)=> total+num);
console.log(reduceNumber);

// =============================================
// class of a Dog
class Dog{
		constructor(name, age, breed){
			this.name = name;
			this.age = age;
			this.breed = breed;
		}
	}
const myDog = new Dog();
myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachsund";
console.log(myDog);





